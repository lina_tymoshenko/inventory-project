export interface IFilter {
  id?: any;
  name?: string;
  imageUrl?: string;
}

export const defaultValue: Readonly<IFilter> = {
  id: '',
  name: '',
  imageUrl: ''
};
