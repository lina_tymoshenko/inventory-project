export interface IRoom {
  id?: any;
  name?: string;
  imageUrl?: string;
}

export const defaultValue: Readonly<IRoom> = {
  id: '',
  name: '',
  imageUrl: ''
};
