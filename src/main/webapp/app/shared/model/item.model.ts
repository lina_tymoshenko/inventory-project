export interface IItem {
  id?: any;
  name?: string;
  imageUrl?: string;
}

export const defaultValue: Readonly<IItem> = {
  id: '',
  name: '',
  imageUrl: ''
};
