import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { IItem, defaultValue } from 'app/shared/model/item.model';

export const ACTION_TYPES = {
  FETCH_ITEMS: 'itemManagement/FETCH_ITEMS',
  FETCH_ITEM: 'itemManagement/FETCH_ITEM',
  CREATE_ITEM: 'itemManagement/CREATE_ITEM',
  UPDATE_ITEM: 'itemManagement/UPDATE_ITEM',
  DELETE_ITEM: 'itemManagement/DELETE_ITEM',
  RESET: 'itemManagement/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  items: [] as ReadonlyArray<IItem>,
  item: defaultValue,
  updating: false,
  updateSuccess: false,
  totalItems: 0
};

export type ItemManagementState = Readonly<typeof initialState>;

export default (state: ItemManagementState = initialState, action): ItemManagementState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_ITEMS):
    case REQUEST(ACTION_TYPES.FETCH_ITEM):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_ITEM):
    case REQUEST(ACTION_TYPES.UPDATE_ITEM):
    case REQUEST(ACTION_TYPES.DELETE_ITEM):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_ITEMS):
    case FAILURE(ACTION_TYPES.FETCH_ITEM):
    case FAILURE(ACTION_TYPES.CREATE_ITEM):
    case FAILURE(ACTION_TYPES.UPDATE_ITEM):
    case FAILURE(ACTION_TYPES.DELETE_ITEM):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_ITEMS):
      return {
        ...state,
        loading: false,
        items: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10)
      };
    case SUCCESS(ACTION_TYPES.FETCH_ITEM):
      return {
        ...state,
        loading: false,
        item: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_ITEM):
    case SUCCESS(ACTION_TYPES.UPDATE_ITEM):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        item: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_ITEM):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        item: defaultValue
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/items';
export const getItems: ICrudGetAllAction<IItem> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_ITEMS,
    payload: axios.get<IItem>(requestUrl)
  };
};

export const getItem: ICrudGetAction<IItem> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_ITEM,
    payload: axios.get<IItem>(requestUrl)
  };
};

export const createItem: ICrudPutAction<IItem> = item => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_ITEM,
    payload: axios.post(apiUrl, item)
  });
  dispatch(getItems());
  return result;
};

export const updateItem: ICrudPutAction<IItem> = item => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_ITEM,
    payload: axios.put(apiUrl, item)
  });
  dispatch(getItems());
  return result;
};

export const deleteItem: ICrudDeleteAction<IItem> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_ITEM,
    payload: axios.delete(requestUrl)
  });
  dispatch(getItems());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
