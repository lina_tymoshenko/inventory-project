import React, { useState, useEffect} from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Table } from 'reactstrap';
import { Translate, getSortState } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';
import { getItems, updateItem } from './item-management.reducer';
import { IRootState } from 'app/shared/reducers';

export interface IItemManagementProps extends StateProps, DispatchProps, RouteComponentProps<{}> {}

export const ItemManagement = (props: IItemManagementProps) => {
  const [pagination, setPagination] = useState(getSortState(props.location, ITEMS_PER_PAGE));

  useEffect(() => {
    props.getItems(pagination.activePage - 1, pagination.itemsPerPage, `${pagination.sort},${pagination.order}`);
    props.history.push(`${location.pathname}?page=${pagination.activePage}&sort=${pagination.sort},${pagination.order}`);
  }, [pagination]);

  const sort = p => () =>
    setPagination({
      ...pagination,
      order: pagination.order === 'asc' ? 'desc' : 'asc',
      sort: p
    });

  const { items, match } = props;

  return (
    <div>
      <h2 id="item-management-page-heading">
        <Translate contentKey="itemManagement.home.title">Items</Translate>
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          <Translate contentKey="itemManagement.home.createLabel">Create a new item</Translate>
        </Link>
      </h2>
      <Table responsive striped>
        <thead>
        <tr>
          <th className="hand" onClick={sort('id')}>
            <Translate contentKey="global.field.id">ID</Translate>
            <FontAwesomeIcon icon="sort" />
          </th>
          <th className="hand" onClick={sort('name')}>
            <Translate contentKey="itemManagement.name">Item Name</Translate>
            <FontAwesomeIcon icon="sort" />
          </th>
          <th className="hand" onClick={sort('imageUrl')}>
            <Translate contentKey="itemManagement.imageUrl">Item Image URL</Translate>
            <FontAwesomeIcon icon="sort" />
          </th>
        </tr>
        </thead>
        <tbody>
          {items.map((item, i) => (
            <tr id={item.id} key={`item-${i}`}>
              <td>
                <Button tag={Link} to={`${match.url}/${item.id}`} color="link" size="sm">
                  {item.id}
                </Button>
              </td>
              <td>{item.name}</td>
              <td>
                <img src={item.imageUrl} alt={"Item image"} className="table__image" />
              </td>
              <td className="text-right">
                <div className="btn-group flex-btn-group-container">
                  <Button tag={Link} to={`${match.url}/${item.id}/edit`} color="primary" size="sm">
                    <FontAwesomeIcon icon="pencil-alt" />{' '}
                    <span className="d-none d-md-inline">
                      <Translate contentKey="entity.action.edit">Edit</Translate>
                    </span>
                  </Button>
                  &nbsp;
                  <Button tag={Link} to={`${match.url}/${item.id}/delete`} color="danger" size="sm">
                    <FontAwesomeIcon icon="trash" />{' '}
                    <span className="d-none d-md-inline">
                      <Translate contentKey="entity.action.delete">Delete</Translate>
                    </span>
                  </Button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  items: storeState.itemManagement.items,
  totalItems: storeState.itemManagement.totalItems
});

const mapDispatchToProps = { getItems, updateItem };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ItemManagement);
