import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import ItemManagementUpdate from './item-management-update';
import ItemManagement from './item-management';
import ItemManagementDeleteDialog from './item-management-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ItemManagementUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ItemManagementUpdate} />
      <ErrorBoundaryRoute path={match.url} component={ItemManagement} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ItemManagementDeleteDialog} />
  </>
);

export default Routes;
