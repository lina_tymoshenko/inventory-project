import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Label, Row, Col } from 'reactstrap';
import { AvForm, AvGroup, AvField } from 'availity-reactstrap-validation';
import { Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { getItem, updateItem, createItem, reset } from './item-management.reducer';
import { IRootState } from 'app/shared/reducers';

export interface IItemManagementUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ItemManagementUpdate = (props: IItemManagementUpdateProps) => {
  const [state, setState] = useState({isNew: !props.match.params || !props.match.params.id, image: ""});

  const handleItemImageChange = (e) => {
    setState({...state, image: e.target.value});
  };

  useEffect(() => {
    if (state.isNew) {
      props.reset();
    } else {
      props.getItem(props.match.params.id);
    }
    return () => props.reset();
  }, []);

  const handleClose = () => {
    props.history.push('/admin/item-management');
  };

  const saveItem = (event, values) => {
    if (state.isNew) {
      props.createItem(values);
    } else {
      props.updateItem(values);
    }
    handleClose();
  };

  const isInvalid = false;
  const { item, loading, updating } = props;

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h1>
            <Translate contentKey="itemManagement.home.createOrEditLabel">Create or edit an Item</Translate>
          </h1>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm onValidSubmit={saveItem}>
              {item.id ? (
                <AvGroup>
                  <Label for="id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvField type="text" className="form-control" name="id" required readOnly value={item.id} />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label for="name">
                  <Translate contentKey="itemManagement.name">Item Name</Translate>
                </Label>
                <AvField
                  type="text"
                  className="form-control"
                  name="name"
                  validate={{
                    required: {
                      value: true,
                      errorMessage: translate('global.messages.validate.name.required')
                    },
                    maxLength: {
                      value: 50,
                      errorMessage: translate('global.messages.validate.name.maxLength', { max: 50 })
                    }
                  }}
                  value={item.name}
                />
              </AvGroup>
              <AvGroup>
                <Label for="imageUrl">
                  <Translate contentKey="itemManagement.imageUrl">Item Image URL</Translate>
                </Label>
                <AvField
                  type="text"
                  className="form-control"
                  name="imageUrl"
                  validate={{
                    required: {
                      value: true,
                      errorMessage: translate('global.messages.validate.imageUrl.required')
                    },
                    maxLength: {
                      value: 1000,
                      errorMessage: translate('global.messages.validate.imageUrl.maxLength', { max: 1000 })
                    },
                    minLength: {
                      value: 5,
                      errorMessage: translate('global.messages.validate.imageUrl.minLength', { min: 5 })
                    }
                  }}
                  onChange={handleItemImageChange}
                  value={item.imageUrl}
                />
                <img src={state.image || item.imageUrl} alt={"Item image"} className="form__image" />
              </AvGroup>
              <Button tag={Link} to="/admin/item-management" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" type="submit" disabled={isInvalid || updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  item: storeState.itemManagement.item,
  loading: storeState.itemManagement.loading,
  updating: storeState.itemManagement.updating
});

const mapDispatchToProps = { getItem, updateItem, createItem, reset };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ItemManagementUpdate);
