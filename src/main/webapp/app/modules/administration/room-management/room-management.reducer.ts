import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { IRoom, defaultValue } from 'app/shared/model/room.model';

export const ACTION_TYPES = {
  FETCH_ROOMS: 'roomManagement/FETCH_ROOMS',
  FETCH_ROOM: 'roomManagement/FETCH_ROOM',
  CREATE_ROOM: 'roomManagement/CREATE_ROOM',
  UPDATE_ROOM: 'roomManagement/UPDATE_ROOM',
  DELETE_ROOM: 'roomManagement/DELETE_ROOM',
  RESET: 'roomManagement/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  rooms: [] as ReadonlyArray<IRoom>,
  room: defaultValue,
  updating: false,
  updateSuccess: false,
  totalItems: 0
};

export type RoomManagementState = Readonly<typeof initialState>;

export default (state: RoomManagementState = initialState, action): RoomManagementState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_ROOMS):
    case REQUEST(ACTION_TYPES.FETCH_ROOM):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_ROOM):
    case REQUEST(ACTION_TYPES.UPDATE_ROOM):
    case REQUEST(ACTION_TYPES.DELETE_ROOM):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_ROOMS):
    case FAILURE(ACTION_TYPES.FETCH_ROOM):
    case FAILURE(ACTION_TYPES.CREATE_ROOM):
    case FAILURE(ACTION_TYPES.UPDATE_ROOM):
    case FAILURE(ACTION_TYPES.DELETE_ROOM):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_ROOMS):
      return {
        ...state,
        loading: false,
        rooms: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10)
      };
    case SUCCESS(ACTION_TYPES.FETCH_ROOM):
      return {
        ...state,
        loading: false,
        room: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_ROOM):
    case SUCCESS(ACTION_TYPES.UPDATE_ROOM):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        room: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_ROOM):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        room: defaultValue
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/rooms';
export const getRooms: ICrudGetAllAction<IRoom> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_ROOMS,
    payload: axios.get<IRoom>(requestUrl)
  };
};

export const getRoom: ICrudGetAction<IRoom> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_ROOM,
    payload: axios.get<IRoom>(requestUrl)
  };
};

export const createRoom: ICrudPutAction<IRoom> = room => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_ROOM,
    payload: axios.post(apiUrl, room)
  });
  dispatch(getRooms());
  return result;
};

export const updateRoom: ICrudPutAction<IRoom> = room => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_ROOM,
    payload: axios.put(apiUrl, room)
  });
  dispatch(getRooms());
  return result;
};

export const deleteRoom: ICrudDeleteAction<IRoom> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_ROOM,
    payload: axios.delete(requestUrl)
  });
  dispatch(getRooms());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
