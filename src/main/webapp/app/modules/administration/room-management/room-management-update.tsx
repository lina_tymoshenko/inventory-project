import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Label, Row, Col } from 'reactstrap';
import { AvForm, AvGroup, AvField } from 'availity-reactstrap-validation';
import { Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { getRoom, updateRoom, createRoom, reset } from './room-management.reducer';
import { IRootState } from 'app/shared/reducers';

export interface IRoomManagementUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const RoomManagementUpdate = (props: IRoomManagementUpdateProps) => {
  const [state, setState] = useState({isNew: !props.match.params || !props.match.params.id, image: ""});

  const handleRoomImageChange = (e) => {
    setState({...state, image: e.target.value});
  };

  useEffect( () => {
    if (state.isNew) {
      props.reset();
    } else {
      props.getRoom(props.match.params.id);
    }
    return () => props.reset();
  }, []);

  const handleClose = () => {
    props.history.push( '/admin/room-management');
  };

  const saveRoom = (event, values) => {
    if (state.isNew) {
      props.createRoom(values);
    } else {
      props.updateRoom(values);
    }
    handleClose();
  };

  const isInvalid = false;
  const { room, loading, updating } = props;

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h1>
            <Translate contentKey="roomManagement.home.createOrEditLabel">Create or edit a Room</Translate>
          </h1>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm onValidSubmit={saveRoom}>
              {room.id ? (
                <AvGroup>
                  <Label for="id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvField type="text" className="form-control" name="id" required readOnly value={room.id} />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label for="name">
                  <Translate contentKey="roomManagement.name">Room Name</Translate>
                </Label>
                <AvField
                  type="text"
                  className="form-control"
                  name="name"
                  validate={{
                    required: {
                      value: true,
                      errorMessage: translate('global.messages.validate.name.required')
                    },
                    maxLength: {
                      value: 50,
                      errorMessage: translate('global.messages.validate.name.maxLength', { max: 50 })
                    }
                  }}
                  value={room.name}
                />
              </AvGroup>
              <AvGroup>
                <Label for="imageUrl">
                  <Translate contentKey="roomManagement.imageUrl">Room Image URL</Translate>
                </Label>
                <AvField
                  type="text"
                  className="form-control"
                  name="imageUrl"
                  validate={{
                    required: {
                      value: true,
                      errorMessage: translate('global.messages.validate.imageUrl.required')
                    },
                    maxLength: {
                      value: 1000,
                      errorMessage: translate('global.messages.validate.imageUrl.maxLength', { max: 1000 })
                    },
                    minLength: {
                      value: 5,
                      errorMessage: translate('global.messages.validate.imageUrl.minLength', { min: 5 })
                    }
                  }}
                  onChange={handleRoomImageChange}
                  value={room.imageUrl}
                />
                <img src={state.image || room.imageUrl} alt={"Room image"} className="form__image" />
              </AvGroup>
              <Button tag={Link} to="/admin/room-management" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" type="submit" disabled={isInvalid || updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  room: storeState.roomManagement.room,
  loading: storeState.roomManagement.loading,
  updating: storeState.roomManagement.updating
});

const mapDispatchToProps = { getRoom, updateRoom, createRoom, reset };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(RoomManagementUpdate);
