import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Table } from 'reactstrap';
import { Translate, getSortState } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';
import { getRooms, updateRoom } from './room-management.reducer';
import { IRootState } from 'app/shared/reducers';

export interface IRoomManagementProps extends StateProps, DispatchProps, RouteComponentProps<{}> {}

export const RoomManagement = (props: IRoomManagementProps) => {
  const [pagination, setPagination] = useState(getSortState(props.location, ITEMS_PER_PAGE));

  useEffect(() => {
    props.getRooms(pagination.activePage - 1, pagination.itemsPerPage, `${pagination.sort},${pagination.order}`);
    props.history.push(`${props.location.pathname}?page=${pagination.activePage}&sort=${pagination.sort},${pagination.order}`);
  }, [pagination]);

  const sort = p => () =>
    setPagination({
      ...pagination,
      order: pagination.order === 'asc' ? 'desc' : 'asc',
      sort: p
    });

  const { rooms, match } = props;

  return (
    <div>
      <h2 id="room-management-page-heading">
        <Translate contentKey="roomManagement.home.title">Rooms</Translate>
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          <Translate contentKey="roomManagement.home.createLabel">Create a new room</Translate>
        </Link>
      </h2>
      <Table responsive striped>
        <thead>
          <tr>
            <th className="hand" onClick={sort('id')}>
              <Translate contentKey="global.field.id">ID</Translate>
              <FontAwesomeIcon icon="sort" />
            </th>
            <th className="hand" onClick={sort('name')}>
              <Translate contentKey="roomManagement.name">Room Name</Translate>
              <FontAwesomeIcon icon="sort" />
            </th>
            <th className="hand" onClick={sort('imageUrl')}>
              <Translate contentKey="roomManagement.imageUrl">Room Image URL</Translate>
              <FontAwesomeIcon icon="sort" />
            </th>
          </tr>
        </thead>
        <tbody>
          {rooms.map((room, i) => (
            <tr id={room.id} key={`room-${i}`}>
              <td>
                <Button tag={Link} to={`${match.url}/${room.id}`} color="link" size="sm">
                  {room.id}
                </Button>
              </td>
              <td>{room.name}</td>
              <td>
                <img src={room.imageUrl} alt={"Room image"} className="table__image" />
              </td>
              <td className="text-right">
                <div className="btn-group flex-btn-group-container">
                  <Button tag={Link} to={`${match.url}/${room.id}/edit`} color="primary" size="sm">
                    <FontAwesomeIcon icon="pencil-alt" />{' '}
                    <span className="d-none d-md-inline">
                      <Translate contentKey="entity.action.edit">Edit</Translate>
                    </span>
                  </Button>
                  &nbsp;
                  <Button tag={Link} to={`${match.url}/${room.id}/delete`} color="danger" size="sm">
                    <FontAwesomeIcon icon="trash" />{' '}
                    <span className="d-none d-md-inline">
                      <Translate contentKey="entity.action.delete">Delete</Translate>
                    </span>
                  </Button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  rooms: storeState.roomManagement.rooms,
  totalItems: storeState.roomManagement.totalItems
});

const mapDispatchToProps = { getRooms, updateRoom };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(RoomManagement);
