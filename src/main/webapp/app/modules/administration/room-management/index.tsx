import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import RoomManagementUpdate from './room-management-update';
import RoomManagement from './room-management';
import RoomManagementDeleteDialog from './room-management-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={RoomManagementUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={RoomManagementUpdate} />
      <ErrorBoundaryRoute path={match.url} component={RoomManagement} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={RoomManagementDeleteDialog} />
  </>
);

export default Routes;
