import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Table } from 'reactstrap';
import { Translate, getSortState } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';
import { getFilters, updateFilter } from './filter-management.reducer';
import { IRootState } from 'app/shared/reducers';

export interface IFilterManagementProps extends StateProps, DispatchProps, RouteComponentProps<{}> {}

export const FilterManagement = (props: IFilterManagementProps) => {
  const [pagination, setPagination] = useState(getSortState(props.location, ITEMS_PER_PAGE));

  useEffect(() => {
    props.getFilters(pagination.activePage - 1, pagination.itemsPerPage, `${pagination.sort},${pagination.order}`);
    props.history.push(`${location.pathname}?page=${pagination.activePage}&sort=${pagination.sort},${pagination.order}`);
  }, [pagination]);

  const sort = p => () =>
    setPagination({
      ...pagination,
      order: pagination.order === 'asc' ? 'desc' : 'asc',
      sort: p
    });

  const { filters, match } = props;

  return (
    <div>
      <h2 id="filter-management-page-heading">
        <Translate contentKey="filterManagement.home.title">Filters</Translate>
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          <Translate contentKey="filterManagement.home.createLabel">Create a new filter</Translate>
        </Link>
      </h2>
      <Table responsive striped>
        <thead>
        <tr>
          <th className="hand" onClick={sort('id')}>
            <Translate contentKey={"global.field.id"}>ID</Translate>
            <FontAwesomeIcon icon="sort" />
          </th>
          <th className="hand" onClick={sort('name')}>
            <Translate contentKey="filterManagement.name">Filter Name</Translate>
            <FontAwesomeIcon icon="sort" />
          </th>
          <th className="hand" onClick={sort('imageUrl')}>
            <Translate contentKey="filterManagement.imageUrl">Filter Image URL</Translate>
            <FontAwesomeIcon icon="sort" />
          </th>
        </tr>
        </thead>
        <tbody>
          {filters.map((filter, i) => (
            <tr id={filter.id} key={`filter-${i}`}>
              <td>
                <Button tag={Link} to={`${match.url}/${filter.id}`} color="link" size="sm">
                  {filter.id}
                </Button>
              </td>
              <td>{filter.name}</td>
              <td>
                <img src={filter.imageUrl} alt={"Filter image"} className="table__image" />
              </td>
              <td className="text-right">
                <div className="btn-group flex-btn-group-container">
                  <Button tag={Link} to={`${match.url}/${filter.id}/edit`} color="primary" size="sm">
                    <FontAwesomeIcon icon="pencil-alt" />{' '}
                    <span className="d-none d-md-inline">
                      <Translate contentKey="entity.action.edit">Edit</Translate>
                    </span>
                  </Button>
                  &nbsp;
                  <Button tag={Link} to={`${match.url}/${filter.id}/delete`} color="danger" size="sm">
                    <FontAwesomeIcon icon="trash" />{' '}
                    <span className="d-none d-md-inline">
                      <Translate contentKey="entity.action.delete">Delete</Translate>
                    </span>
                  </Button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
    filters: storeState.filterManagement.filters,
    totalItems: storeState.filterManagement.totalItems
});

const mapDispatchToProps = { getFilters, updateFilter };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(FilterManagement);
