import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Label, Row, Col } from 'reactstrap';
import { AvForm, AvGroup, AvField } from 'availity-reactstrap-validation';
import { Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { getFilter, updateFilter, createFilter, reset } from './filter-management.reducer';
import { IRootState } from 'app/shared/reducers';

export interface IFilterManagementUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const FilterManagementUpdate = (props: IFilterManagementUpdateProps) => {
  const [state, setState] = useState({isNew: !props.match.params || !props.match.params.id, image: ""});

  const handleFilterImageChange = (e) => {
    setState({...state, image: e.target.value});
  };

  useEffect( () => {
    if (state.isNew) {
      props.reset();
    } else {
      props.getFilter(props.match.params.id);
    }
    return () => props.reset();
  }, []);

  const handleClose = () => {
    props.history.push('/admin/filter-management');
  };

  const saveFilter = (event, values) => {
    if (state.isNew) {
      props.createFilter(values);
    } else {
      props.updateFilter(values);
    }
    handleClose();
  };

  const isInvalid = false;
  const { filter, loading, updating } = props;

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h1>
            <Translate contentKey="filterManagement.home.createOrEditLabel">Create or edit a Filter</Translate>
          </h1>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm onValidSubmit={saveFilter}>
              {filter.id ? (
                <AvGroup>
                  <Label for="id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvField type="text" className="form-control" name="id" required readOnly value={filter.id} />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label for="name">
                  <Translate contentKey="filterManagement.name">Filter Name</Translate>
                </Label>
                <AvField
                  type="text"
                  className="form-control"
                  name="name"
                  validate={{
                    required: {
                      value: true,
                      errorMessage: translate('global.messages.validate.name.required')
                    },
                    maxLength: {
                      value: 50,
                      errorMessage: translate('global.messages.validate.name.maxLength', { max: 50 })
                    }
                  }}
                  value={filter.name}
                />
              </AvGroup>
              <AvGroup>
                <Label for="imageUrl">
                  <Translate contentKey="filterManagement.imageUrl">Filter Image URL</Translate>
                </Label>
                <AvField
                  type="text"
                  className="form-control"
                  name="imageUrl"
                  validate={{
                    required: {
                      value: true,
                      errorMessage: translate('global.messages.validate.imageUrl.required')
                    },
                    maxLength: {
                      value: 1000,
                      errorMessage: translate('global.messages.validate.imageUrl.maxLength', { max: 1000 })
                    },
                    minLength: {
                      value: 5,
                      errorMessage: translate('global.messages.validate.imageUrl.minLength', { min: 5 })
                    }
                  }}
                  onChange={handleFilterImageChange}
                  value={filter.imageUrl}
                />
                <img src={state.image || filter.imageUrl} alt={"Filter image"} className="form__image" />
              </AvGroup>
              <Button tag={Link} to="/admin/filter-management" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" type="submit" disabled={isInvalid || updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  filter: storeState.filterManagement.filter,
  loading: storeState.filterManagement.loading,
  updating: storeState.filterManagement.updating
});

const mapDispatchToProps = { getFilter, updateFilter, createFilter, reset };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(FilterManagementUpdate);
