import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { IFilter, defaultValue } from 'app/shared/model/filter.model';

export const ACTION_TYPES = {
  FETCH_FILTERS: 'filterManagement/FETCH_FILTERS',
  FETCH_FILTER: 'filterManagement/FETCH_FILTER',
  CREATE_FILTER: 'filterManagement/CREATE_FILTER',
  UPDATE_FILTER: 'filterManagement/UPDATE_FILTER',
  DELETE_FILTER: 'filterManagement/DELETE_FILTER',
  RESET: 'filterManagement/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  filters: [] as ReadonlyArray<IFilter>,
  filter: defaultValue,
  updating: false,
  updateSuccess: false,
  totalItems: 0
};

export type FilterManagementState = Readonly<typeof initialState>;

export default (state: FilterManagementState = initialState, action): FilterManagementState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_FILTERS):
    case REQUEST(ACTION_TYPES.FETCH_FILTER):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_FILTER):
    case REQUEST(ACTION_TYPES.UPDATE_FILTER):
    case REQUEST(ACTION_TYPES.DELETE_FILTER):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_FILTERS):
    case FAILURE(ACTION_TYPES.FETCH_FILTER):
    case FAILURE(ACTION_TYPES.CREATE_FILTER):
    case FAILURE(ACTION_TYPES.UPDATE_FILTER):
    case FAILURE(ACTION_TYPES.DELETE_FILTER):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_FILTERS):
      return {
        ...state,
        loading: false,
        filters: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10)
      };
    case SUCCESS(ACTION_TYPES.FETCH_FILTER):
      return {
        ...state,
        loading: false,
        filter: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_FILTER):
    case SUCCESS(ACTION_TYPES.UPDATE_FILTER):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        filter: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_FILTER):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        filter: defaultValue
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/filters';
export const getFilters: ICrudGetAllAction<IFilter> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_FILTERS,
    payload: axios.get<IFilter>(requestUrl)
  };
};

export const getFilter: ICrudGetAction<IFilter> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_FILTER,
    payload: axios.get<IFilter>(requestUrl)
  };
};

export const createFilter: ICrudPutAction<IFilter> = filter => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_FILTER,
    payload: axios.post(apiUrl, filter)
  });
  dispatch(getFilters());
  return result;
};

export const updateFilter: ICrudPutAction<IFilter> = filter => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_FILTER,
    payload: axios.put(apiUrl, filter)
  });
  dispatch(getFilters());
  return result;
};

export const deleteFilter: ICrudDeleteAction<IFilter> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_FILTER,
    payload: axios.delete(requestUrl)
  });
  dispatch(getFilters());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
