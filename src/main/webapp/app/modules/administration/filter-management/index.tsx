import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import FilterManagementUpdate from './filter-management-update';
import FilterManagement from './filter-management';
import FilterManagementDeleteDialog from './filter-management-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={FilterManagementUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={FilterManagementUpdate} />
      <ErrorBoundaryRoute path={match.url} component={FilterManagement} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={FilterManagementDeleteDialog} />
  </>
);

export default Routes;
