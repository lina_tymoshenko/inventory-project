package com.inventory.app.service.dto;

import com.inventory.app.domain.Room;

import javax.validation.constraints.Size;

/**
 * A DTO representing a room
 */
public class RoomDTO {

    private Long id;

    @Size(max = 255)
    private String name;

    @Size(max = 1000)
    private String imageUrl;

    public RoomDTO() {
        // need for jackson
    }

    public RoomDTO(Room room) {
        this.id = room.getId();
        this.name = room.getName();
        this.imageUrl = room.getImageUrl();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
