package com.inventory.app.service.dto;

import com.inventory.app.domain.Filter;
import com.inventory.app.domain.Room;

import javax.validation.constraints.Size;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A DTO representing a filter
 */
public class FilterDTO {

    private Long id;

    @Size(max = 255)
    private String name;

    @Size(max = 1000)
    private String imageUrl;

    private List<RoomDTO> rooms;

    public FilterDTO() {
        // need for jackson
    }

    public FilterDTO(Filter filter) {
        this.id = filter.getId();
        this.name = filter.getName();
        this.imageUrl = filter.getImageUrl();
        this.rooms = filter.getRooms()
            .stream()
            .map(RoomDTO::new)
            .collect(Collectors.toList());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<RoomDTO> getRooms() {
        return rooms;
    }

    public void setRooms(List<RoomDTO> rooms) {
        this.rooms = rooms;
    }
}
