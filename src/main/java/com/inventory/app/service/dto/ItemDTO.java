package com.inventory.app.service.dto;

import com.inventory.app.domain.Item;

import javax.validation.constraints.Size;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A DTO representing a item
 */
public class ItemDTO {

    private Long id;

    @Size(max = 255)
    private String name;

    @Size(max = 1000)
    private String imageUrl;

    private Set<FilterDTO> filters;

    public ItemDTO() {
        // need for jackson
    }

    public ItemDTO(Item item) {
        this.id = item.getId();
        this.name = item.getName();
        this.imageUrl = item.getImageUrl();
        this.filters = item.getFilters()
            .stream()
            .map(FilterDTO::new)
            .collect(Collectors.toSet());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Set<FilterDTO> getFilters() {
        return filters;
    }

    public void setFilters(Set<FilterDTO> filters) {
        this.filters = filters;
    }
}
