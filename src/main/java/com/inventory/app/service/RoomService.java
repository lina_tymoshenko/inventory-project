package com.inventory.app.service;

import com.inventory.app.domain.Room;
import com.inventory.app.repository.RoomRepository;
import com.inventory.app.service.dto.RoomDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service class for managing rooms.
 */
@Service
@Transactional
public class RoomService {

    private final Logger log = LoggerFactory.getLogger(RoomService.class);

    private final RoomRepository roomRepository;

    public RoomService(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    public Room create(RoomDTO roomDTO) {
        Room room = new Room();

        room.setName(roomDTO.getName());
        room.setImageUrl(roomDTO.getImageUrl());

        roomRepository.save(room);

        log.debug("Created Information for Room: {}", room);

        return room;
    }

    public Page<RoomDTO> getAllRooms(Pageable pageable) {
        return roomRepository.findAll(pageable).map(RoomDTO::new);
    }

    public Optional<RoomDTO> find(Long id) {
        return roomRepository.findById(id).map(RoomDTO::new);
    }

    public Optional<RoomDTO> updateRoom(RoomDTO roomDTO) {
        return Optional.of(roomRepository
            .findById(roomDTO.getId()))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(room -> {
                room.setName(roomDTO.getName());
                room.setImageUrl(roomDTO.getImageUrl());

                log.debug("Changed Information for Room: {}", room);
                return room;
            })
            .map(RoomDTO::new);
    }

    public void deleteRoom(Long id) {
        roomRepository.findOneById(id).ifPresent(room -> {
            roomRepository.delete(room);
            log.debug("Deleted Room: {}", room);
        });
    }
}
