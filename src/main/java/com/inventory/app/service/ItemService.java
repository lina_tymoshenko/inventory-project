package com.inventory.app.service;

import com.inventory.app.domain.Filter;
import com.inventory.app.domain.Item;
import com.inventory.app.repository.FilterRepository;
import com.inventory.app.repository.ItemRepository;
import com.inventory.app.service.dto.FilterDTO;
import com.inventory.app.service.dto.ItemDTO;
import com.inventory.app.service.dto.RoomDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Service class for managing items.
 */
@Service
@Transactional
public class ItemService {

    private final Logger log = LoggerFactory.getLogger(ItemService.class);

    private final ItemRepository itemRepository;

    private final FilterRepository filterRepository;

    public ItemService(ItemRepository itemRepository, FilterRepository filterRepository) {
        this.itemRepository = itemRepository;
        this.filterRepository = filterRepository;
    }

    public Item create(ItemDTO itemDTO) {
        Item item = new Item();

        item.setName(itemDTO.getName());
        item.setImageUrl(itemDTO.getImageUrl());

        setFilters(itemDTO, item);

        itemRepository.save(item);

        log.debug("Created Information for Item: {} ", item);

        return item;
    }

    private void setFilters(ItemDTO itemDTO, Item item) {
        Set<Long> filterIds = itemDTO.getFilters()
            .stream()
            .map(FilterDTO::getId)
            .collect(Collectors.toSet());

        List<Filter> filters = filterRepository.findAllById(filterIds);

        if (!filters.isEmpty()) {
            item.setFilters(filters);
        }
    }

    public Page<ItemDTO> getAllItems(Pageable pageable) {
        return itemRepository.findAll(pageable).map(ItemDTO::new);
    }

    public Optional<ItemDTO> find(Long id) {
        return itemRepository.findById(id).map(ItemDTO::new);
    }

    public Optional<ItemDTO> updateItem(ItemDTO itemDTO) {
        return Optional.of(itemRepository
        .findById(itemDTO.getId()))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(item -> {
                item.setName(itemDTO.getName());
                item.setImageUrl(itemDTO.getImageUrl());

                setFilters(itemDTO, item);

                log.debug("Changed Information for Item: {}", item);

                return item;
            })
            .map(ItemDTO::new);
    }

    public void deleteItem(Long id) {
        itemRepository.findOneById(id).ifPresent(item -> {
            itemRepository.delete(item);
            log.debug("Delete Item: {}", item);
        });
    }
}
