package com.inventory.app.service;

import com.inventory.app.domain.Filter;
import com.inventory.app.domain.Room;
import com.inventory.app.repository.FilterRepository;
import com.inventory.app.repository.RoomRepository;
import com.inventory.app.service.dto.FilterDTO;
import com.inventory.app.service.dto.RoomDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Service class for managing filters.
 */
@Service
@Transactional
public class FilterService {

    private final Logger log = LoggerFactory.getLogger(FilterService.class);

    private final FilterRepository filterRepository;

    private final RoomRepository roomRepository;

    public FilterService(FilterRepository filterRepository, RoomRepository roomRepository) {
        this.filterRepository = filterRepository;
        this.roomRepository = roomRepository;
    }

    public Filter create(FilterDTO filterDTO) {
        Filter filter = new Filter();

        filter.setName(filterDTO.getName());
        filter.setImageUrl(filterDTO.getImageUrl());
        setRooms(filterDTO, filter);

        filterRepository.save(filter);

        log.debug("Create Information for Filter: {}", filter);

        return filter;
    }

    private void setRooms(FilterDTO filterDTO, Filter filter) {
        Set<Long> roomIds = filterDTO.getRooms()
            .stream()
            .map(RoomDTO::getId)
            .collect(Collectors.toSet());

        List<Room> rooms = roomRepository.findAllById(roomIds);

        if (!rooms.isEmpty()) {
            filter.setRooms(rooms);
        }
    }

    public Page<FilterDTO> getAllFilters(Pageable pageable) {
        return filterRepository.findAll(pageable).map(FilterDTO::new);
    }

    public Optional<FilterDTO> find(Long id) {
        return filterRepository.findById(id).map(FilterDTO::new);
    }

    public Optional<FilterDTO> updateFilter(FilterDTO filterDTO) {
        return Optional.of(filterRepository
        .findById(filterDTO.getId()))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(filter -> {
                filter.setName(filterDTO.getName());
                filter.setImageUrl(filterDTO.getImageUrl());

                setRooms(filterDTO, filter);

                log.debug("Changed Information for Filter: {}", filter);

                return filter;
            })
            .map(FilterDTO::new);
    }

    public void deleteFilter(Long id) {
        filterRepository.findOneById(id).ifPresent(filter -> {
            filterRepository.delete(filter);
            log.debug("Delete Filter: {}", filter);
        });
    }
}
