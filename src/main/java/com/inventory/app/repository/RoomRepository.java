package com.inventory.app.repository;

import com.inventory.app.domain.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data JPA repository for the {@link Room} entity.
 */
@Repository
public interface RoomRepository extends JpaRepository<Room, Long> {

    Optional<Room> findOneById(Long id);
}
