package com.inventory.app.repository;

import com.inventory.app.domain.Filter;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface FilterRepository extends JpaRepository<Filter, Long> {

    Optional<Filter> findOneById(Long id);
}
