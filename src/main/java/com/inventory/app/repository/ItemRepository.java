package com.inventory.app.repository;

import com.inventory.app.domain.Item;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ItemRepository extends JpaRepository<Item, Long> {

    Optional<Item> findOneById(Long id);
}
