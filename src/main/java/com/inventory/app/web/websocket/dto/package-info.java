/**
 * Data Access Objects used by WebSocket services.
 */
package com.inventory.app.web.websocket.dto;
