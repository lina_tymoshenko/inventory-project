package com.inventory.app.web.rest;

import com.inventory.app.domain.Filter;
import com.inventory.app.security.AuthoritiesConstants;
import com.inventory.app.service.FilterService;
import com.inventory.app.service.dto.FilterDTO;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class FilterResource {

    private final Logger log = LoggerFactory.getLogger(FilterResource.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FilterService filterService;

    public FilterResource(FilterService filterService) {
        this.filterService = filterService;
    }

    @PostMapping("/filters")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Filter> createFilter(@Valid @RequestBody FilterDTO filterDTO) throws URISyntaxException {
        log.debug("REST request to save Filter : {}", filterDTO);

        Filter newFilter = filterService.create(filterDTO);

        return ResponseEntity.created(new URI("/api/rooms/" + newFilter.getId()))
            .headers(HeaderUtil.createAlert(applicationName, "filterManagement.created", newFilter.getName()))
            .body(newFilter);
    }

    @GetMapping("/filters")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<List<FilterDTO>> getAllFilters(Pageable pageable) {
        final Page<FilterDTO> page = filterService.getAllFilters(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/filters/{id:.+}")
    public ResponseEntity<FilterDTO> getFilterById(
        @PathVariable Long id
    ) {
        return ResponseUtil.wrapOrNotFound(filterService.find(id));
    }

    @PutMapping("/filters")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<FilterDTO> updateFilter(@Valid @RequestBody FilterDTO filterDTO) {
        log.debug("REST request to update Filter : {}", filterDTO);

        Optional<FilterDTO> updatedFilter = filterService.updateFilter(filterDTO);

        return ResponseUtil.wrapOrNotFound(updatedFilter,
            HeaderUtil.createAlert(applicationName, "filterManagement.updated", filterDTO.getName()));
    }

    @DeleteMapping("/filters/{id:.+}")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> deleteFilter(@PathVariable Long id) {
        log.debug("REST request to delete Filter: {}", id);

        filterService.deleteFilter(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createAlert(applicationName, "filterManagement.deleted", id.toString())).build();
    }
}
