package com.inventory.app.web.rest;

import com.inventory.app.domain.Item;
import com.inventory.app.security.AuthoritiesConstants;
import com.inventory.app.service.ItemService;
import com.inventory.app.service.dto.ItemDTO;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ItemResource {

    private final Logger log = LoggerFactory.getLogger(ItemResource.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ItemService itemService;

    public ItemResource(ItemService itemService) {
        this.itemService = itemService;
    }

    @PostMapping("/items")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Item> createItem(@Valid @RequestBody ItemDTO itemDTO) throws URISyntaxException {
        log.debug("REST request to save Item : {}", itemDTO);

        Item newItem = itemService.create(itemDTO);

        return ResponseEntity.created(new URI("/api/items/" + newItem.getId()))
            .headers(HeaderUtil.createAlert(applicationName, "itemManagement.created", newItem.getName()))
            .body(newItem);
    }

    @GetMapping("/items")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<List<ItemDTO>> getAllItems(Pageable pageable) {
        final Page<ItemDTO> page = itemService.getAllItems(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/items/{id:.+}")
    public ResponseEntity<ItemDTO> getItemById(
        @PathVariable Long id
    ) {
        return ResponseUtil.wrapOrNotFound(itemService.find(id));
    }

    @PutMapping("/items")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<ItemDTO> updateItem(@Valid @RequestBody ItemDTO itemDTO) {
        log.debug("REST request to update Item : {}", itemDTO);

        Optional<ItemDTO> updatedItem = itemService.updateItem(itemDTO);

        return ResponseUtil.wrapOrNotFound(updatedItem,
            HeaderUtil.createAlert(applicationName, "itemManagement.updated", itemDTO.getName()));
    }

    @DeleteMapping("/items/{id:.+}")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> deleteItem(@PathVariable Long id) {
        log.debug("REST request to delete Item: {}", id);
        itemService.deleteItem(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createAlert(applicationName,  "itemManagement.deleted", id.toString())).build();
    }
}
