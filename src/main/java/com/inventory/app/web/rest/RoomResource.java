package com.inventory.app.web.rest;

import com.inventory.app.domain.Room;
import com.inventory.app.security.AuthoritiesConstants;
import com.inventory.app.service.RoomService;
import com.inventory.app.service.dto.RoomDTO;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class RoomResource {

    private final Logger log = LoggerFactory.getLogger(RoomResource.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RoomService roomService;

    public RoomResource(RoomService roomService) {
        this.roomService = roomService;
    }

    @PostMapping("/rooms")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Room> createRoom(@Valid @RequestBody RoomDTO roomDTO) throws URISyntaxException {
        log.debug("REST request to save Room : {}", roomDTO);

        Room newRoom = roomService.create(roomDTO);

        return ResponseEntity.created(new URI("/api/rooms/" + newRoom.getId()))
            .headers(HeaderUtil.createAlert(applicationName, "roomManagement.created", newRoom.getName()))
            .body(newRoom);
    }

    @GetMapping("/rooms")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<List<RoomDTO>> getAllRooms(Pageable pageable) {
        final Page<RoomDTO> page = roomService.getAllRooms(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/rooms/{id:.+}")
    public ResponseEntity<RoomDTO> getRoomById(
        @PathVariable Long id
    ) {
        return ResponseUtil.wrapOrNotFound(roomService.find(id));
    }

    @PutMapping("/rooms")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<RoomDTO> updateRoom(@Valid @RequestBody RoomDTO roomDTO) {
        log.debug("REST request to update Room : {}", roomDTO);

        Optional<RoomDTO> updatedRoom = roomService.updateRoom(roomDTO);

        return ResponseUtil.wrapOrNotFound(updatedRoom,
            HeaderUtil.createAlert(applicationName, "roomManagement.updated", roomDTO.getName()));
    }

    @DeleteMapping("/rooms/{id:.+}")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> deleteRoom(@PathVariable Long id) {
        log.debug("REST request to delete Room: {}", id);
        roomService.deleteRoom(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createAlert(applicationName,  "roomManagement.deleted", id.toString())).build();
    }
}
